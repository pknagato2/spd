#!/usr/bin/python
import sys
from subprocess import check_output

def hilite(string, status, bold):
    attr = []
    if status:
        # green
        attr.append('32')
    else:
        # red
        attr.append('31')
    if bold:
        attr.append('1')
    return '\x1b[%sm%s\x1b[0m' % (';'.join(attr), string)


case = sys.argv[1]

for i in range(1, 10):
    out = check_output(['./a.out', case + str(i) + ".DAT"])
    out = [int(s) for s in out.split() if s.isdigit()]
    out = out[0]

    with open(case + str(i) + ".OUT", 'r') as f:
        read_data = f.read()

    print("TEST: " + case + str(i))
    print("EXPECTED: " + str(read_data) + " ACTUAL: " + str(out))
    if str(read_data) == str(out):
        print(hilite("PASSED", True, True))
    else:
        print(hilite("FAILED", False, True))
